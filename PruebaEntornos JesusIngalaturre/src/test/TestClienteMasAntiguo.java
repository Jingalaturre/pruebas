package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TestClienteMasAntiguo {
	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	static ArrayList<Cliente> listaClientes;
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
		listaClientes = new ArrayList<Cliente>();
	}
	
	@Test 
	public void clienteMasAntiguo() {
	//Hacer dos Clientes y ver que me devuelve el mas antiguo.
	Cliente esperado = new Cliente("1221");
	esperado.setFechaAlta(LocalDate.parse("2012-12-12"));
	gestorPruebas.altaCliente(esperado);
	
	actual = new Cliente("12");
	actual.setFechaAlta(LocalDate.now());
	gestorPruebas.altaCliente(actual);
	
	assertSame(esperado,gestorPruebas.clienteMasAntiguo());
	
	}
	
	@Test 
	public void dosCLientesConMismaFecha() {
	
	//Dos clientes con la misma fecha de alta
	//Da fallo porque ambos se dieron de alta al mismo tiempo.
		
	Cliente esperado = new Cliente("1221");
	esperado.setFechaAlta(LocalDate.now());
	gestorPruebas.altaCliente(esperado);
	
	actual = new Cliente("12");
	actual.setFechaAlta(LocalDate.now());
	gestorPruebas.altaCliente(actual);
	
	assertEquals(esperado.getFechaAlta(),gestorPruebas.clienteMasAntiguo());
	
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	

}
