package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import clases.Factura;
import clases.GestorContabilidad;

public class TestFacturaBorrarFactura {
	static Factura facturaPrueba;
	static GestorContabilidad gestor;
	
	@BeforeClass
	public static void prepararClasePruebas(){
		facturaPrueba = new Factura();
		gestor = new GestorContabilidad();
	}
	
	@Test 
	public void borrarFactura() {
		
		//Agregar una factura y eliminarla
		//Da null
		
		Factura esperado = new Factura("12");
		gestor.crearFactura(esperado);
		gestor.eliminarFactura("12");
		esperado = gestor.buscarFactura("12");
		assertNull(esperado);
		
	}
	
	@Test
	public void borrarClienteInexistente() {
		
		//Borrar cliente que no existe.
		//Da null.
		
		gestor.eliminarFactura("12");
		
		Factura esperado = gestor.buscarFactura("12");
		
		assertNull(esperado);
		
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	

}
