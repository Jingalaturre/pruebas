package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TestClienteBorrarCliente {
	
	static	GestorContabilidad gestorPruebas;
		Cliente actual;
		static ArrayList<Cliente> listaClientes;
		@BeforeClass
		public static void inicializarClase(){
			gestorPruebas = new GestorContabilidad();
			listaClientes = new ArrayList<Cliente>();
		}
		
	@Test 
	public void borrarCliente() {
		
		//A�ades un cliente, lo eliminas
		//Da null.
		
		Cliente esperado = new Cliente("12");
		gestorPruebas.altaCliente(esperado);
		gestorPruebas.eliminarCliente("12");
		esperado = gestorPruebas.buscarCliente("12");
		assertNull(esperado);
		
	}
	
	@Test
	public void borrarClienteInexistente() {
		
		//A�ades un cliente que no existe.
		//Da null.
		
		gestorPruebas.eliminarCliente("12");
		
		Cliente esperado = gestorPruebas.buscarCliente("12");
		
		assertNull(esperado);
		
	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	

}
