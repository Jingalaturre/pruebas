package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TestsClienteTest {
	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	static ArrayList<Cliente> listaClientes;
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
		listaClientes = new ArrayList<Cliente>();
	}
	
	

	
	@Test
	public void testBuscarClienteInexistenteSinClientes() {
		//Buscas un dni sin estar asignado a un cliente y tiene que dar null.
		String dni = "123";
		
		Cliente actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteExistente(){
		//Cliente existente, lo a�adimos y lo buscamos.
		//El cliente que buscamos tiene que ser el mismo que el que hemos creado.
		Cliente esperado = new Cliente("1234");
		
		gestorPruebas.getListaClientes().add(esperado);

		actual = gestorPruebas.buscarCliente("1234");

		assertSame(esperado, actual);
	}
	
	@Test
	public void testBuscarClienteInexistenteConClientes(){	
		String dni = "12";
		//Buscas cliente inexistente ,da null.
		actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteHabiendoVariosClientes(){
		//Creamos varios clientes y buscamos uno de ellos.
		Cliente esperado = new Cliente("1221");
		gestorPruebas.getListaClientes().add(esperado);
		
		String dniABuscar = "1212";
		esperado = new Cliente(dniABuscar);
		
		gestorPruebas.getListaClientes().add(esperado);		
		actual = gestorPruebas.buscarCliente(dniABuscar);
		
		assertSame(esperado, actual);
	}
	
	@Test
	public void testAltaCliente(){
		//Creamos un cliente y lo a�adimos a un ArrayList.
		//Tiene que dar lo mismo que buscamos.
		Cliente esperado = new Cliente("12345");
		gestorPruebas.altaCliente(esperado);
		
		actual = gestorPruebas.buscarCliente(esperado.getDni());
		
		assertEquals(esperado,actual);
	}
	
	@Test
	public void testAltaClienteDosDniIguales(){
		
		//Damos de alta a dos clientes y los comparamos.
		//Tiene que dar fallo porque no pueden crearse dos iguales.
		
		Cliente esperado = new Cliente("121231124");
		gestorPruebas.altaCliente(esperado);
		
		Cliente actual = new Cliente("121231124");
		gestorPruebas.altaCliente(esperado);
		
		assertEquals(esperado, actual);
	}
		
		
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}



}
