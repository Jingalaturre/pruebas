package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestClienteFacturaAsignar {
	static Factura facturaPrueba;
	static GestorContabilidad gestor;
	
	@BeforeClass
	public static void prepararClasePruebas(){
		facturaPrueba = new Factura();
		gestor = new GestorContabilidad();
	}
	
	@Test
    public void asignarFacturaACliente() {
		
		//asignamos mediante el dni y el codigo una factura a un cliente
		//se espera que el cliente de la factura corresponda al cliente creado
		
        Factura factura = new Factura("7");
        Cliente cliente = new Cliente("7");
        
        gestor.altaCliente(cliente);
        gestor.crearFactura(factura);
        
        gestor.asignarClienteAFactura("7", "7");

        Cliente esperado = factura.getCliente();
        Cliente actual = cliente;
        
        assertSame(esperado, actual);
    }
	
	@Test
	public void asignarFacturaAClienteYaExistente() {
			
			//Creamos una factua con cliente ya existente
			//Tiene que dar null.
		
		 	Factura factura = new Factura("7");
	        Cliente cliente = new Cliente("7");
	        
	        gestor.altaCliente(cliente);
	        gestor.crearFactura(factura);
	        
	        gestor.asignarClienteAFactura("7", "7");

	        Cliente esperado = factura.getCliente();
	       
	        
	        assertNull(esperado);
	}
	
	@Test
	public void cantidadFacturasPorCliente() {
		
			//Se crea una factura con cliente
			//Se espera que el total de facturas sea 1.
		
			Factura factura = new Factura("8");
			Cliente cliente = new Cliente("8");
			
			gestor.altaCliente(cliente);
	        gestor.crearFactura(factura);
	        
	        gestor.asignarClienteAFactura("8", "8");
	        
	        int esperado = 1;
	        int actual = gestor.cantidadFacturasPorCliente("8");
	        assertEquals(esperado,actual);
	        
			
	}
	
	@Test
	public void cantidadFacturasPorClienteYaExistente() {
		
			//Se crea un dos facturas con cliente iguales
			//Como solo se tiene que poder agregar uno el total esperado es 1.
		
			Factura factura = new Factura("9");
			Cliente cliente = new Cliente("9");
			
			gestor.altaCliente(cliente);
	        gestor.crearFactura(factura);
	        
	        gestor.asignarClienteAFactura("9", "9");
	        
	        Factura factura2 = new Factura("9");
			Cliente cliente2 = new Cliente("9");
			
			gestor.altaCliente(cliente2);
	        gestor.crearFactura(factura2);
	        
	        gestor.asignarClienteAFactura("9", "9");
	        
	        int esperado = 1;
	        int actual = gestor.cantidadFacturasPorCliente("9");
	        assertEquals(esperado,actual);
	        
			
	}
	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	

}
