package test;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;


public class TestsFactura {
	
	static Factura facturaPrueba;
	static GestorContabilidad gestor;
	
	@BeforeClass
	public static void prepararClasePruebas(){
		facturaPrueba = new Factura();
		gestor = new GestorContabilidad();
	}
	
	
	@Test
	public void testBuscarFacturaInexistenteSinFacturas() {
	//Un codigo sin asignar a una factura se utiliza para buscar la factura.
	//Da null.
		String codigo = "121212";
		
		Factura actual = gestor.buscarFactura(codigo);	
		assertNull(actual);
	}

	@Test
	public void testBuscarFacturaExistente(){
		//Crear una factura exisatente y buscarla por su codigo.
		//Da lo mismo la factura buscada que la creada.
		
		Factura esperado = new Factura("12");
		
		gestor.getListaFacturas().add(esperado);

		facturaPrueba = gestor.buscarFactura("12");

		assertSame(esperado, facturaPrueba);
	}
	
	@Test
	public void testBuscarFacturaInexistenteConFacturas(){	
		
		//Teniendo facturas creadas agregar una factura inexistente.
		//Da null.
		String codigo = "122";
		
		facturaPrueba = gestor.buscarFactura(codigo);	
		assertNull(facturaPrueba);
	}

	@Test
	public void testBuscarFacturaHabiendoVariosFacturas(){
		
		//Creamos varias facturas y buscamos una de ellas.
		Factura esperado = new Factura("1");
		gestor.getListaFacturas().add(esperado);
		String codigo = "1212";
		esperado = new Factura(codigo);
		gestor.getListaFacturas().add(esperado);
		
		facturaPrueba = gestor.buscarFactura(codigo);
		
		assertSame(esperado, facturaPrueba);
	}
	
	@Test
	public void testCrearFactura(){
		
		//Creamos una factura y la a�admos a un arrayList
		Factura esperado = new Factura("123");
		gestor.crearFactura(esperado);
		
		facturaPrueba = gestor.buscarFactura(esperado.getCodigoFactura());
		
		assertEquals(esperado,facturaPrueba);
	}
	
	@Test
	public void testCrearFacturaDosCodigosIguales(){
		//Damos de alta a dos facturas y los comparamos.
		//Tiene que dar fallo porque no pueden crearse dos iguales.
		Factura esperado = new Factura("12");
		gestor.crearFactura(esperado);
		
		String codigo = "12";
		esperado = new Factura(codigo);
		
		gestor.crearFactura(esperado);
		
		facturaPrueba = gestor.buscarFactura(codigo);
		
		
		assertSame(esperado,facturaPrueba);
	}
	
	@Test
	public void facturaMasCara() {
		
		//a�adimos dos facturas le damos valores de cantidad de productos y su precio.
		//Calculamos el total y miramos que la mayor sea la factura mas cara.
		Factura esperado = new Factura("1");
		esperado.setCantidad(2);
		esperado.setPrecioUnidad(5.0F);
		esperado.calcularPrecioTotal();
		
		gestor.crearFactura(esperado);
		Factura esperado2 = new Factura("2");
		
		esperado2.setCantidad(2);
		esperado2.setPrecioUnidad(2);
		esperado2.calcularPrecioTotal();
		
		gestor.crearFactura(esperado2);
		esperado = gestor.buscarFactura("1");
		
		assertEquals(esperado, gestor.facturaMasCara());
		
	}
	
	@Test
	public void facturaMasCaraInexistente() {
		//No hay factuar mas cara, tiene que dar null.
	
		assertNull(gestor.facturaMasCara());
		
	}
	
	@Test
	public void dosfacturasMasCaras() {
	
		//Dos facturas con el mismo precio.
		//En el caso de que sean las mas caras dara error porque las dos son igual de caras.
		Factura esperado = new Factura("3");
		esperado.setCantidad(2);
		esperado.setPrecioUnidad(4.0F);
		esperado.calcularPrecioTotal();
		
		gestor.crearFactura(esperado);
		Factura esperado2 = new Factura("4");
		
		esperado2.setCantidad(2);
		esperado2.setPrecioUnidad(4.0F);
		esperado2.calcularPrecioTotal();
		
		gestor.crearFactura(esperado2);
		
		esperado = gestor.buscarFactura("3");
		esperado2 = gestor.buscarFactura("4");
		
		assertEquals(esperado2, gestor.facturaMasCara());
		
	}
	
	@Test
	public void calcularFacturacionAnual() {
		
		//A�ado una factura para y le pongo una fecha
		//Calculamos la facturacion anual que se ha sacado en el a�o de la factura
		//Tiene que dar lo mismo que lo que vale esa factura.
		
		
		Factura factura = new Factura("5");
		factura.setFecha(LocalDate.parse("2000-12-12"));
		factura.setCantidad(1);
		factura.setPrecioUnidad(5.00F);
		factura.calcularPrecioTotal();
		gestor.crearFactura(factura);
		
		 float actual = gestor.calcularFacturacionAnual(2000);
		 float esperado = 5F;
		assertEquals(esperado,actual,0.0);
		
	}
	
	@Test
	public void calcularFacturacionAnualSinProductos() {
		
		//Al no tener productos es lo mismo que el anterior pero se espera 0 en el resultado.
		
		
		Factura esperado = new Factura("6");
		esperado.setFecha(LocalDate.parse("2000-12-12"));
		esperado.setCantidad(0);
		esperado.setPrecioUnidad(5);
		esperado.calcularPrecioTotal();
		gestor.crearFactura(esperado);
		
		float esperado2 = gestor.calcularFacturacionAnual(2000);
		
		assertEquals(esperado2,0,0.0);
		
	}
	
	
	@Test 
	public void calcularPrecio(){
		
		//Calculamos precio total cantidad productos * precio producto. 
		//5*2, se espera 10.
		Factura factura = new Factura();
		factura.setCantidad(5);
		factura.setPrecioUnidad(2);
		float actual = factura.getCantidad()*factura.getPrecioUnidad();
		float esperado = 10.0F;
		
		assertEquals(esperado, actual,0.0);
		
	}
	
	@Test 
	public void calcularPrecioNegativo(){
		
		//Calculamos precio total cantidad productos * precio producto. 
		//2*-4, se espera -8.
		Factura factura = new Factura();
		factura.setCantidad(2);
		factura.setPrecioUnidad(-4);
		float actual = factura.getCantidad()*factura.getPrecioUnidad();
		float esperado = -8.0F;
		
		assertEquals(esperado, actual,0.0);
		
	}
	
	@Test 
	public void calcularPrecioSinProductos(){
		
		//Calculamos precio total cantidad productos * precio producto. 
		//0*4, se espera 0.
		Factura factura = new Factura();
		factura.setCantidad(0);
		factura.setPrecioUnidad(4);
		float actual = factura.getCantidad()*factura.getPrecioUnidad();
		float esperado = 0.0F;
		
		assertEquals(esperado, actual,0.0);
		
	}
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
}
