package clases;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad(){
		
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();
	}
	 
	
	
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}


	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}



	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}



	public Cliente buscarCliente(String dni){
		
		for (int i = 0; i < listaClientes.size(); i++) {
			if(listaClientes.get(i).getDni().equals(dni)){
				return listaClientes.get(i);
			}
		}
		
		return null;
	}
	
	public Factura buscarFactura(String codigo){
		
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getCodigoFactura().equals(codigo)){
				return listaFacturas.get(i);
			}
		}
		
		return null;
	}
	
	public void altaCliente(Cliente cliente){
		String dni = cliente.getDni();
		 if(buscarCliente(dni) == null){
			
			listaClientes.add(cliente);
		}
		
	}
	
	public void crearFactura(Factura factura){
		String codigo = factura.getCodigoFactura();
		if(buscarFactura(codigo) == null){
			
			listaFacturas.add(factura);
		}
	}
	
	
	public Cliente clienteMasAntiguo() {
		if (!listaClientes.isEmpty()) {
			Cliente clienteMasAntiguo = listaClientes.get(0);
			for (int i = 0; i < listaClientes.size(); i++) {
				if (listaClientes.get(i).getFechaAlta().isBefore(clienteMasAntiguo.getFechaAlta())) {
					clienteMasAntiguo = listaClientes.get(i);
				}
				
				
			}
			return clienteMasAntiguo;
		}
		return null;
	}
	
	
	
	public Factura facturaMasCara(){
		Float precio = 0.0F;
		Factura facturaMasCara = null;
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i).calcularPrecioTotal() > precio){
				precio = listaFacturas.get(i).calcularPrecioTotal();
				facturaMasCara = listaFacturas.get(i);
			}
		}
		
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i)!= null){
				return facturaMasCara;
			}
		}
		
		return null;
		
	}
	
	
	public float calcularFacturacionAnual(int anno){
		
		float precioAnno = 0.0F;
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getFecha().getYear()==anno ){
				precioAnno= precioAnno + listaFacturas.get(i).calcularPrecioTotal();
				
			}
			
		}
		
		
		return precioAnno;		
	
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		if (buscarCliente(dni) != null && buscarFactura(codigoFactura) != null) {
			buscarFactura(codigoFactura).setCliente(buscarCliente(dni));
		}
	}
	
	//falta
	public int cantidadFacturasPorCliente(String dni) {
		int total = 0;
		if (buscarCliente(dni) != null) {
			for (int i = 0; i <listaFacturas.size();i++) {
				
			 
				if (listaFacturas.get(i).getCliente() == buscarCliente(dni)) {
					total++;
				}
			}
		}
		return total;
	}
	
	
	public void eliminarCliente(String dni){
		
		for (int i = 0; i < listaClientes.size(); i++) {
			if(buscarCliente(dni) != null){
			listaClientes.remove(i);
			
			}
		}
		
	}
	
	
	public void eliminarFactura(String codigoFactura){
		
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(buscarFactura(codigoFactura) != null){
			listaFacturas.remove(i);
		}
		}}
}
